#include "../lib/HelloWorld.H"

#include <v8.h>
#include <node.h>

using namespace v8;

class NativeModule : node::ObjectWrap {
private:
	const char * greet() {
		return "Greetings from Native Code!";
	}
public:
	NativeModule() {}
	~NativeModule() {}
// Our constructor
    static v8::Persistent<FunctionTemplate> persistent_function_template;

    static void Init(Handle<Object> target) {
      v8::HandleScope scope; // used by v8 for garbage collection

      // Our constructor
      v8::Local<FunctionTemplate> local_function_template = v8::FunctionTemplate::New(New);
      NativeModule::persistent_function_template = v8::Persistent<FunctionTemplate>::New(local_function_template);
      NativeModule::persistent_function_template->InstanceTemplate()->SetInternalFieldCount(1); // 1 since this is a constructor function

      // Our methods
      NODE_SET_PROTOTYPE_METHOD(NativeModule::persistent_function_template, "greet", Greet);

      // Binding our constructor function to the target variable
      target->Set(String::NewSymbol("nativemodule"), NativeModule::persistent_function_template->GetFunction());
    }

    static Handle<Value> New( const Arguments& args ) {
    	HandleScope scope;
    	
    	NativeModule* nativemodule_instance = new NativeModule();
    	nativemodule_instance->Wrap( args.This());

    	return args.This();
    }

    static Handle<Value> Greet( const Arguments& args ) {
    	NativeModule* nativemodule_instance = node::ObjectWrap::Unwrap<NativeModule>( args.This() );

    	return v8::String::New( nativemodule_instance->greet() );
    }
};

/*
 * WARNING: Boilerplate code ahead.
 * 
 * See https://www.cloudkick.com/blog/2010/aug/23/writing-nodejs-native-extensions/ & http://www.freebsd.org/cgi/man.cgi?query=dlsym
 *  
 * Thats it for actual interfacing with v8, finally we need to let Node.js know how to dynamically load our code. 
 * Because a Node.js extension can be loaded at runtime from a shared object, we need a symbol that the dlsym function can find, 
 * so we do the following:  
 */

v8::Persistent<FunctionTemplate> NativeModule::persistent_function_template;
extern "C" { // Cause of name mangling in C++, we use extern C here
  static void init(Handle<Object> target) {
    NativeModule::Init(target);
  }
  // @see http://github.com/ry/node/blob/v0.2.0/src/node.h#L101
  NODE_MODULE(nativemodule, init);
}
